activate / source your ln-environment:

    . ~/path/to/ln_prefix/ln_env.sh

edit `Makefile` at the very top, set `LN_PREFIX` to match your install prefix:

    LN_PREFIX ?= ~/path/to/ln_prefix

(this will be used for compiling/linking against that LN)

now start the ln-manager:

    ./test.lnc

now start one of the `test_provider*` processes and then one of the `test_client*`'s.

## how to get LN?

    git clone --recursive https://gitlab.com/links_and_nodes/links_and_nodes.git
    cd links_and_nodes
    ./scripts/build_debian12.sh ~/path/to/ln_prefix

now you have LN in `~/path/to/ln_prefix`.
