# todo: set this to the install prefix of your LN
LN_PREFIX ?= $(HOME)/path/to/ln_prefix

CXXFLAGS = --std=c++20 -Wall -I$(LN_PREFIX)/include
LIBS = $(LN_PREFIX)/lib/libln.so

TARGETS=send_traj_client send_traj_provider send_traj_provider2

MD_DIR=mds
MDS=test/send_traj

all: $(TARGETS)

src/ln_messages.h: $(foreach md,$(MDS),$(MD_DIR)/$(md))
	ln_generate -o $@ --md-dir $(MD_DIR) $(MDS)

%: src/%.cpp src/ln_messages.h Makefile
	g++ -o $@ $< $(CXXFLAGS) $(LIBS)

clean:
	-rm $(TARGETS) src/ln_messages.h
