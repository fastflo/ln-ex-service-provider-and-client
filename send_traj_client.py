import numpy as np
import links_and_nodes as ln

class MyClient:
    def __init__(self):
        self.clnt = ln.client("test")
        self._send_traj_svc = self.clnt.get_service("send_traj", "test/send_traj")

    def send_traj(self, v):
        svc = self._send_traj_svc
        new_point = svc.req.new_desired_trajectory_point_t_packet

        svc.req.points = []

        p = new_point()
        p.desired_com_position = np.array([0.0565982, -0.006321, 0.6], dtype=np.float64)
        p.desired_com_velocity = np.array([0., 0., 0.], dtype=np.float64)
        p.desired_left_foot_pose = np.array([v, 0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_right_foot_pose = np.array([-0.0115, -0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_contact = np.uint16(2)  # 2: double contact
        svc.req.points.append(p)

        p = new_point()
        p.desired_com_position = np.array([0.0565982, -0.006321, 0.59], dtype=np.float64)
        p.desired_com_velocity = np.array([0., 0., 0.], dtype=np.float64)
        p.desired_left_foot_pose = np.array([2*v, 0.33, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_right_foot_pose = np.array([-0.0115, -0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_contact = np.uint16(1)  # 1: for testing
        svc.req.points.append(p)

        svc.call()
        print("got status %s" % svc.resp.status)

if __name__ == "__main__":
    app = MyClient()
    app.send_traj(3.14)
    app.send_traj(6.28)
