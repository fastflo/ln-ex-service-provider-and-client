#!/bin/bash

log_file=run_test.log

ln_manager -c test.lnc --without-gui --isolated-test --console-exec "source run_test.cmd" 2>&1 | tee "${log_file}"

# provide return value:
grep -e "got status: 42" "${log_file}" || exit 1
grep -e "got status 42 for 50000 points" "${log_file}" || exit 2

exit 0
