import sys

import numpy as np
import links_and_nodes as ln

class MyClient:
    def __init__(self):
        self.clnt = ln.client("test")
        self._send_traj_svc = self.clnt.get_service("send_traj", "test/send_traj")

    def send_traj(self, v, N):
        svc = self._send_traj_svc
        new_point = svc.req.new_desired_trajectory_point_t_packet

        svc.req.points = []

        print("preparing %d points..." % N)
        for i in range(N):
            p = new_point()
            p.desired_com_position = np.array([0.0565982, -0.006321, 0.6], dtype=np.float64)
            p.desired_com_velocity = np.array([0., 0., 0.], dtype=np.float64)
            p.desired_left_foot_pose = np.array([(i + 1) / N * v, 0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
            p.desired_right_foot_pose = np.array([-0.0115, -0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
            p.desired_contact = np.uint16(i % 4)
            svc.req.points.append(p)

        print("sending %d points..." % N)
        svc.call()
        print("got status %s for %d points" % (svc.resp.status, N))

if __name__ == "__main__":
    app = MyClient()
    N = int(sys.argv[1])
    app.send_traj(3.14, N)
