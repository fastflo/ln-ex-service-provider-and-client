import pprint

import numpy as np
import links_and_nodes as ln

class MyProvider:
    def __init__(self):
        self.clnt = ln.client("test")
        self._send_traj_svc = self.clnt.get_service_provider("send_traj", "test/send_traj")
        self._send_traj_svc.set_handler(self.on_send_traj)
        self._send_traj_svc.do_register()

    def on_send_traj(self, svc, req, resp):
        print("incoming call:")
        for i, point in enumerate(req.points):
            print("  point %d:\n%s\n" % (i, pprint.pformat(point.dict())))
        resp.status = 42
        svc.respond()
        return 0

    def run(self):
        print("ready")
        while True:
            self.clnt.wait_and_handle_service_group_requests(None)

if __name__ == "__main__":
    app = MyProvider()
    app.run()
