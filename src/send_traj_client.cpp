#include <stdio.h>

#include <vector>

#include "ln_messages.h"

class MyClient
	: test::send_traj_base
{
	ln::client clnt { "test" };
	ln::service* send_traj_svc;
public:
	MyClient() {
		send_traj_svc = clnt.get_service("send_traj", "test/send_traj", test_send_traj_signature);
	}

	int send_test_traj(double v) {
		test_send_traj_t data {};
		std::vector<test_desired_trajectory_point_t> points;

		points.push_back( {
				{ v, 0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1. }, // left_foot_pose
				{ -0.0115, -0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1. }, // right_foot_pose
				{ 0.0565982, -0.006321, 0.6 }, // com_position
				{ 0., 0., 0. }, // com_velocity
				2 // desired_contact: double contact
			});

		points.push_back( {
				{ 2*v, 0.33, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1. }, // left_foot_pose
				{ -0.0115, -0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1. }, // right_foot_pose
				{ 0.0565982, -0.006321, 0.59 }, // com_position
				{ 0., 0., 0. }, // com_velocity
				1 // desired_contact
			});

		data.req.points = &points[0];
		data.req.points_len = points.size();

		send_traj_svc->call(&data);

		printf("got status: %d\n", data.resp.status);
		return data.resp.status;
	}

	int run() {
		send_test_traj(3.14);
		send_test_traj(6.28);
		return 0;
	}
};

int main()
{
	auto app = MyClient();
	return app.run();
}
