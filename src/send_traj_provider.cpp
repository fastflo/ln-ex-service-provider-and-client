#include <stdio.h>

#include <string>
#include <sstream>

#include "ln_messages.h"

template<typename T>
std::string to_string(T* s, std::size_t N) {
	std::stringstream ss;
	for(unsigned int i = 0; i < N; i++) {
		if(i > 0)
			ss << ", ";
		ss << std::to_string(s[i]);
	}
	return ss.str();
}

class MyProvider
	: test::send_traj_base
{
	ln::client clnt { "test" };

public:
	MyProvider() {
		register_send_traj(&clnt, "send_traj");
	}

	int on_send_traj(::ln::service_request& req, test_send_traj_t& data) override {
		printf("incoming call:\n");
		for(unsigned int i = 0; i < data.req.points_len; i++) {
			auto& point = data.req.points[i];
			printf("point %d: desired_*: left_foot_pose: %s, ... contact: %d\n",
			       i,
			       to_string(point.desired_left_foot_pose, 12).c_str(),
			       point.desired_contact
			);
		}
		data.resp.status = 42;
		req.respond();
		return 0;
	}

	int run() {
		printf("ready\n");
		while (true)
			clnt.wait_and_handle_service_group_requests(NULL);
	}
};

int main()
{
	auto app = MyProvider();
	return app.run();
}
