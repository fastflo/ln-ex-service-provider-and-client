#ifndef LN_MESSAGES_H
#define LN_MESSAGES_H

#include <stdint.h>
#include <ln/ln.h>

/* test/desired_trajectory_point */
#include "ln/packed.h"
struct LN_PACKED test_desired_trajectory_point_struct;
typedef struct test_desired_trajectory_point_struct test_desired_trajectory_point_t;
struct LN_PACKED test_desired_trajectory_point_struct {
	double desired_left_foot_pose[12];
	double desired_right_foot_pose[12];
	double desired_com_position[3];
	double desired_com_velocity[3];
	uint16_t desired_contact;
};
#include "ln/endpacked.h"
/* test/send_traj */
#define test_send_traj_signature "uint32_t 4 1,[double 8 12,double 8 12,double 8 3,double 8 3,uint16_t 2 1]* 242 1|uint16_t 2 1"
#include "ln/packed.h"
typedef struct LN_PACKED {
	struct LN_PACKED test_send_traj_request_t {
		uint32_t points_len;
		test_desired_trajectory_point_t* points;
	} req;
	struct LN_PACKED test_send_traj_response_t {
		uint16_t status;
	} resp;
#ifdef __cplusplus
	typedef test_send_traj_request_t request_t;
	typedef test_send_traj_response_t response_t;
#endif

} test_send_traj_t;
#include "ln/endpacked.h"

#ifdef __cplusplus
#ifdef LN_MD_PREFIX_NS
namespace LN_MD_PREFIX_NS {
#endif
namespace test {

typedef test_send_traj_t send_traj_t;

class send_traj_base {
public:
	virtual ~send_traj_base() {
		unregister_send_traj();
	}
private:
	static int cb(::ln::client&, ::ln::service_request& req, void* user_data) {
		send_traj_base* self = (send_traj_base*)user_data;
		send_traj_t data;
		req.set_data(&data, test_send_traj_signature);
		memset(&data.resp, 0, sizeof(data.resp));
		return self->on_send_traj(req, data);
	}
protected:
	::ln::service* svc;
	
	send_traj_base() : svc(NULL) {};
	
	void unregister_send_traj() {
		if(svc) {
			svc->clnt->release_service(svc);
			svc = NULL;
		}
	}
	void register_send_traj(::ln::client* clnt, const std::string service_name, const char* group_name=NULL) {
		svc = clnt->get_service_provider(
			service_name,
			"test/send_traj", 
			test_send_traj_signature);
		svc->set_handler(&cb, this);
		svc->do_register(group_name);
	}
	virtual int on_send_traj(::ln::service_request& req, send_traj_t& data) {
		fprintf(stderr, "ERROR: no virtual int on_test_send_traj() handler overloaded for service send_traj!\\n");
		return 1;
	}
};
} // namespace test
#ifdef LN_MD_PREFIX_NS
}
#endif
#endif
#endif // LN_MESSAGES_H
