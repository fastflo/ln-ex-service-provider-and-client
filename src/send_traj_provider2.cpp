#include <stdio.h>

#include <string>
#include <sstream>

#include "ln_messages.h"

template<typename T>
std::string to_string(T* s, std::size_t N) {
	std::stringstream ss;
	for(unsigned int i = 0; i < N; i++) {
		if(i > 0)
			ss << ", ";
		ss << std::to_string(s[i]);
	}
	return ss.str();
}

class MyProvider
// here, not using the generated base class: `test::send_traj_base` -> we get all the // boilerplate!
{
	ln::client clnt { "test" };
	ln::service* send_traj_svc;                                                             // boilerplate!

	static int _on_send_traj(ln::client& clnt, ln::service_request& req, void* user_data) { // boilerplate!
		// just translate this callback into calling a method of the given instance...  // boilerplate!
		return static_cast<MyProvider*>(user_data)->on_send_traj(req);                  // boilerplate!
	}

public:
	MyProvider() {
		send_traj_svc = clnt.get_service_provider("send_traj", "test/send_traj",
							  test_send_traj_signature);            // boilerplate!
		send_traj_svc->set_handler(&_on_send_traj, this); // pass pointer to instance `this` as user-data // boilerplate!
		send_traj_svc->do_register();
	}

	int on_send_traj(::ln::service_request& req) {
		test_send_traj_t data {};                                                       // boilerplate!
		req.set_data(&data, test_send_traj_signature);                                  // boilerplate!

		printf("incoming call:\n");
		for(unsigned int i = 0; i < data.req.points_len; i++) {
			auto& point = data.req.points[i];
			printf("point %d: desired_*: left_foot_pose: %s, ... contact: %d\n",
			       i,
			       to_string(point.desired_left_foot_pose, 12).c_str(),
			       point.desired_contact
			);
		}
		data.resp.status = 42;
		req.respond();
		return 0;
	}

	int run() {
		printf("ready\n");
		while (true)
			clnt.wait_and_handle_service_group_requests(NULL);
	}
};

int main()
{
	auto app = MyProvider();
	return app.run();
}
