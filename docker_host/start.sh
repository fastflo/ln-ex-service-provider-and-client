#!/bin/bash

# check that required image exists
docker image inspect ln/ex-svc-prov-host || ./build.sh

# check that required network exists
docker network inspect netA || docker network create netA

docker run --name ln_ex_debian_12 \
    --privileged \
    -it \
    --network netA \
    --rm \
    --ipc host \
    ln/ex-svc-prov-host \
    "$@"
