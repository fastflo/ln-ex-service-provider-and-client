#!/bin/sh

docker image inspect ln/debian:12 || ./get_ln_docker.sh

exec docker build \
     --network host \
     -t ln/ex-svc-prov-host \
     -f Dockerfile \
     ..
