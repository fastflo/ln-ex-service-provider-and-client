#!/bin/bash

# this will create docker image	`ln/debian:12`

git clone --recursive https://gitlab.com/links_and_nodes/links_and_nodes.git
cd links_and_nodes/docker/debian12
./build.sh
