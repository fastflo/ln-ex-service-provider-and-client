import pprint

import numpy as np
import links_and_nodes as ln

class MyProvider(ln.service_provider):
    def __init__(self):
        self.clnt = ln.client("test")
        ln.service_provider.__init__(self, self.clnt)

        self.wrap_service_provider("send_traj", "test/send_traj")

    def send_traj(self, points):
        print("incoming call:")
        for i, point in enumerate(points):
            print("  point %d:\n%s\n" % (i, pprint.pformat(point.dict())))

        return 42

    def run(self):
        self.handle_service_group_requests()

if __name__ == "__main__":
    app = MyProvider()
    app.run()
