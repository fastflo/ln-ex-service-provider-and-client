import numpy as np
import links_and_nodes as ln

class MyClient(ln.services_wrapper): # less boilerplate with ln.services_wrapper, tries to provide python-method-call API for service-calls
    def __init__(self):
        self.clnt = ln.client("test")
        ln.services_wrapper.__init__(self, self.clnt)
        self._send_traj = self.wrap_service("send_traj", "test/send_traj")
        # we want to get access to that underlying `.svc` object to be able to use the
        # `new_desired_trajectory_point_t_packet()` ctor

    def do_call_with(self, v):
        svc = self._send_traj.svc
        new_point = svc.req.new_desired_trajectory_point_t_packet

        points = []
        p = new_point()
        p.desired_com_position = np.array([0.0565982, -0.006321, 0.6], dtype=np.float64)
        p.desired_com_velocity = np.array([0., 0., 0.], dtype=np.float64)
        p.desired_left_foot_pose = np.array([v, 0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_right_foot_pose = np.array([-0.0115, -0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_contact = np.uint16(2)  # 2: double contact
        points.append(p)

        p = new_point()
        p.desired_com_position = np.array([0.0565982, -0.006321, 0.59], dtype=np.float64)
        p.desired_com_velocity = np.array([0., 0., 0.], dtype=np.float64)
        p.desired_left_foot_pose = np.array([2*v, 0.33, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_right_foot_pose = np.array([-0.0115, -0.11, 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.], dtype=np.float64)
        p.desired_contact = np.uint16(1)  # 1: for testing
        points.append(p)

        status = self.send_traj(points)
        print("got status: %d" % status)

if __name__ == "__main__":
    app = MyClient()
    app.do_call_with(3.14)
    app.do_call_with(6.28)
